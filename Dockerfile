FROM maven:3.6.0-jdk-10

WORKDIR /ngt-codex-qa/
EXPOSE  9001
RUN apt-get update

ENV PUSH_GATE="https://prometheus-us-central1.grafana.net/api/prom"

#timezone

ENV TZ=America/El_Salvador
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone



RUN apt-get update && apt-get install -y --no-install-recommends \
    jq curl git chromium vim nano

# Add hosts file
ADD hosts /root/hosts

RUN curl -SsL https://downloads.gauge.org/stable | sed 's/latest/15176631/g' | sh -s -- --location=/usr/sbin/
RUN gauge install java --version 0.7.1
RUN gauge install json-report
RUN gauge install html-report --version 4.0.6

COPY ./bin/ /ngt-codex-qa/bin/

# Tasker will make sure it runs every 5 minutes
RUN cat /root/hosts >> /etc/hosts
RUN chmod a+x /ngt-codex-qa/bin/*
COPY ./ ./ngt-codex-qa/ 
RUN git clone https://ecortez:Ed202789jcoandr.@git.hightech-corp.com/qa/fomilenio.git

CMD ["sh", "-c", "/ngt-codex-qa/bin/entrypoint.sh"]
