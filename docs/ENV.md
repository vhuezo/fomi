# Environments

## 
```mermaid
graph LR;
    A[Rational Team Concern] -->|RDM Apps Jenkins Flow| a1;
    subgraph SonarQube
    a1(SonarQube)-->a2(Postgresql Database);
    end;
    a1 --> |badges README.md| A;
    a1 --> |slurp|C(agent);
    C -->|metrics|D[prometheus];
    D --> F[GRAFANA Dashboard];
    G[GAUGE Test Case Design ] --> |Source Control|A;
    A --> | Gauge Jenkins Flow|ROBOT;
    ROBOT --> D;
    E[Production Infrastructure]-->Agents
    Agents-->AppDynamics;
    AppDynamics--> F;
```

----


```mermaid
  graph LR;
  cr(Change Request);
  sbx(Sandbox);
  dev(Development);
  iot(Integration);
  pre(Regression);
  prd(Production);
  sbx-->dev;
  dev-->iot;
  iot-->pre;
  pre-->prd;
```

----

### /etc/hosts

```sh
# IOT
10.100.52.102   csr-ameiot.tigoune.com.co

# SBX
10.100.52.101   csr-fixedline.tigoune.com.co
```

----

### Application URLS

https://tigouneco.zendesk.com/hc/es/requests/new?ticket_form_id=360000130687
https://tigopy.zendesk.com/agent/dashboard
Usuario: luis.vasquez@tigoune.com
Contraseña: Tigo2017*


|Env|Tool                                                                |Description                        | Credentials
|---|--------------------------------------------------------------------|-----------------------------------|-------------
|IOT|[CSR Toolbox](http://csr-ameiot.tigoune.com.co:7500)                | CRM Tool                          |fry/fry
|IOT|[POS](http://csr-ameiot.tigoune.com.co:85)                          | Point of sales tool               |anders.a@qvantel.com / salakala
|IOT|[Track'n Trace](http://csr-ameiot.tigoune.com.co:9000)              | Trace orders                      |
|IOT|[Case Mgmnt](http://csr-ameiot.tigoune.com.co:8092/casemanager/)    | Case management tool for customers|
|IOT|[BSS API](http://csr-ameiot.tigoune.com.co:3010)                    | API for accessing components      |
|SBX|[CSR Toolbox](http://csr-fixedline.tigoune.com.co:7500)             | CRM Tool                          |
|SBX|[POS](http://csr-fixedline.tigoune.com.co:85)                       | Point of sales tool               |
|SBX|[Track'n Trace](http://csr-fixedline.tigoune.com.co:9000)           | Trace orders                      |
|SBX|[Case Mgmnt](http://csr-fixedline.tigoune.com.co:8092/casemanager/) | Case management tool for customers|
|SBX|[BSS API](http://csr-fixedline.tigoune.com.co:3010)                 | API for accessing components      |

----

### BSS API

|Environment|Address        |
|-----------|---------------|
| IOT       | https://sandbox.api.tigo.com/v1/tigo/co/tls_two_way/29443 |
| Sandbox   | https://sandbox.api.tigo.com/v1/tigo/co/tls_two_way/19443 |

### Test data

|Type|Id        |Description|
|----|----------|-----------|
|ID  |Lotero45  |           |
|ID  |Lotero52  |           |
|ID  |Lotero50  |           |
