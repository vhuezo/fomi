# Methods in git
This file helps to the developers to clone, pull, push, merge, add, commit and other commands and triks for **Git**.
## Clone the project
To clone thiis project you need to use the next command
```
git clone http://git.hightech-corp.com/millicom/ngt-codex-qa.git
```

## Get project changes

In the git project folder you need to use the next command
```
git pull
```

If you need to checkout a project branch you will use the next command
```
git clone http://git.hightech-corp.com/millicom/ngt-codex-qa.git --branch <name_of_branch>
```
Example:
```
git clone http://git.hightech-corp.com/millicom/ngt-codex-qa.git --branch develop-tigoune
```

## Commit changes

In the git project folder you need to use the nexts commands
```
git add -A
git commit -m "<message>"
git push
```
If someone else makes changes in the project you will need to merge this changes, sometimes the merge is make it automatically but sometimes you will have to make it manually.

Before make the merge you need to use the next commands
```
git add .
git commit -m "<message>"
git push
```

## Reset files to origin

Sometimes you need to reset your project to the remote master 
```
git fetch  --all
git reset  --hard origin/master
git pull
```
> **ProTip:**  Make a backup always.

