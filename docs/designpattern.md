# Design pattern for ngt

- [Design pattern for ngt](#design-pattern-for-ngt)
    - [Fluent java](#fluent-java)
        - [General rules](#general-rules)
        - [Test Specs](#test-specs)
        - [Page Object Model](#page-object-model)
        - [Step Implementation](#step-implementation)
        - [Java](#java)
        - [Examples fluent java with functional representation of the tool](#examples-fluent-java-with-functional-representation-of-the-tool)
    - [Fluid java](#fluid-java)
        - [Lambda functions](#lambda-functions)
        - [Searching](#searching)

## Fluent java

> [Learn more](https://martinfowler.com/bliki/FluentInterface.html) from Martin Fowler

### General rules

### Test Specs

- TestSpecs should be specific and contain goal oriented business language
    - "Click this and that", "type this message" etc is not goal oriented.
    - "Make sure there is a prepaid subscription" is more specific and goal oriented.
- Scenarios should relate to the context of its spec

### Page Object Model

- We create a functional representation of pages, tabs and elements in a fluent manner
- To structure and create maintainable code we use the concept ```application -> [tab ->] function```

### Step Implementation

- ***We do assertions only in step implementation***
- We use *fluent interfaces* to create maintainable code
- We simplify the code close to perfection
  - Dont be shy to use overloading, recursion and lambda functions

### Java
- We basically agree with https://google.github.io/styleguide/javaguide.html


### Examples fluent java with functional representation of the tool
=======

## Fluid java
>>>>>>> Preprod-tigoune

```java
CSRToolBox toolbox = new CSRToolBox(...)

// * Has an order with id "..."
assertEquals("Assert one or more order", toolbox.orderTab().search().results().asJsonPath("$[0].orderId"),
		"3939384");

// * Has no canceled orders between ".." and "..."
assertEquals("Should find exact zero canceled orders", 0,
		toolbox.orderTab().orderBasketOrIdFilter("12334567").orderDateIntervalFilter("07/29/2018", "07/29/2030")
				.orderTypeFilter("all").orderStatusFilter("canceled").search().results().size());

// * Have orders
assertTrue("Should return > 0 orders", Integer.parseInt(toolbox.orderTab().search().orderTypeFilter("all")
		.orderStatusFilter("all").results().asJsonPath("$.length()")) > 0);

// * Has one specific order with reference "..."
assertEquals("Should have exact one completed order with specific reference", "PO_IssueNewSubs_Sandboxvv796",
		toolbox.orderTab().orderDateIntervalFilter("01/01/1970", "07/29/2030").orderTypeFilter("all")
				.orderStatusFilter("completed").search().results().asJsonPath("$[0].orderRef"));
```

```json
{
    "orderId": "2432",
    "orderRef": "PO_IssueNewSubs_Sandboxvv796",
    "createdAt": "2018-08-21T22:40:37.635Z",
    "status": "completed",
    "owner": "25002381 Carlos Lotero45",
    "contact": "",
    "salesInfo": {
        "channel": "retail",
        "salesType": "acquisition",
        "salesPersonId": "fpirajan"
    },
    "orderItems": [
        {
            "productName": "PS_Prepaid_Plan",
            "status": "completed",
            "quantity": 1,
            "user": "Carlos Lotero45",
            "productType": "product",
            "productSubType": "additional",
            "orderItemPrices": [],
            "priority": 1,
            "completedAt": "2018-08-21T22:40:56.894Z",
            "changeable": true
        }
    ],
    "isChangeable": true
}
```

### Lambda functions

[Lambda functions tutorial](https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html)

```java
// get all orders add add to result
els(ordersrepeater).forEach(item -> {
    result.add(angular().retrieve(item, "order"));
});
```

### Searching

```java

// Search result interface from result()
interface SearchResult {

  // Format as json string without parsing
  String asJson();

  // parse and extract with jsonpath format
  // https://github.com/json-path/JsonPath
  String asJsonPath(String jsonPath);

  int size();
}

```
