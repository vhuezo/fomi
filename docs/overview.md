# Overview

> “However, most defects end up costing more than it would have cost to prevent them. Defects are expensive when they occur, both the direct costs of fixing the defects and the indirect costs because of damaged relationships, lost business, and lost development time.”

## Continuous automated testing

<img src="img/continuous-testing.png" alt="drawing" width="60%"/> 

## Architecture/process

```mermaid
  graph LR
    spec(Automated test <br /> Specification)
    uc(Use-case test <br> Specification)
    step(Step <br> Implementation)
    app(Test Subject <br> Execution)
    fb(Feedback <br> and monitoring)
    uc-->spec
    spec-->step
    step-->app
    app-->fb
```

### Scope

- sef-care
- payment gateway (pgw)
- dsp
  

### Use case test specification

<img src="img/reqs1.png" alt="drawing" width="100%"/>

<img src="img/reqs2.png" alt="drawing" width="100%"/>

### Test specification

<img src="img/scenario.png" alt="drawing" width="100%"/>

### API

<img src="img/code.png" alt="drawing" width="60%"/> 


### Architecture

```mermaid
    graph TD
        subgraph Solution
            subgraph duPrivateCloud;
            tr[Test Robot<br>Framework];
            chrome[Chrome<br/>Browser Slave];
            firefox[Firefox<br/>Browser Slave];
            spec((Test<br/>spec));
            tp((Test<br/>plan));
            report((Test<br/>Report));
            prometheus(Prometheus<br/>Metric-server);
            grafana(Grafana<br/>Visualizer);
            jira(Jira issue<br/>Tracker);
            env((env));
            tr---jira;
            spec---tr;
            spec---tp;
            tp---tr;
            tr-->chrome;
            tr-->firefox;
            tr-->report;
            report-->prometheus;
            prometheus-->grafana;
            chrome-->env;
            firefox-->env;
                subgraph Local;
                    env-->app;
                    app(Application<br/>Under testing);
                end
            end
        end

        classDef pink fill:#f8f,stroke:#333,stroke-width:1px;
        class tr,chrome,firefox pink;

        classDef blue fill:#77f,stroke:#333,stroke-width:1px;
        class tp,spec,report blue;

        classDef white fill:#fff,stroke:#333,stroke-width:1px;
        class prometheus,grafana,jira,env white;

        classDef green fill:#cfc,stroke:#333,stroke-width:1px;
        class app green;
```

### Development  flow

#### Flow

```mermaid
    graph LR
        subgraph flow
            subgraph Dev
                testcase1-->specsdev
                testcase2-->specsdev
                testcaseN-->specsdev
                specsdev-->commit
            end
            subgraph Ops
                commit-->start
                start(( ))
                start-->execute
                execute-->report
                report-->publish
                publish-->|5m|start
                publish-->prometheus
                grafana---prometheus
            end
        end
```

### Monitoring

<img src="img/grafana.png" alt="drawing" width="100%"/>

### Products

<img src="img/products.png" alt="drawing" width="100%"/>


### Class diagram

``` mermaid
classDiagram
Spec *-- Scenario
Spec : Text
Spec *-- Context
Context *-- Step
Scenario *-- Step
Scenario : Text

Step --> Status
Status <|--Pass
Status <|--Fail
Status <|--Wait
Step : Text[]
Step --> StepImpl
StepImpl : Function
StepImpl --> Status
StepImpl *-- Page

Page --> Page
Page-->Elements
Page-->Tab
```

