package com.htc.ngt.lambda.qa.selfcare;

import static org.junit.Assert.assertEquals;
import com.htc.ngt.lambda.qa.common.StepImplementation;
import com.htc.ngt.lambda.qa.driver.Driver;
import com.thoughtworks.gauge.Step;

/**
 * CSRToolboxSteps
 */
public class DuSelfcareSteps implements StepImplementation {

        private DuSelfcarePage toolbox;
       
        public DuSelfcareSteps(){    
        }

        @Step("Login Email:<email> and Password: <pass>")
        public DuSelfcarePage goToUrl(String user, String password) {
            this.toolbox = new DuSelfcarePage(Driver.getDriver(true), System.getenv("SELF_CARE_URL")).login(user, password);
            return this.toolbox;
        }

        @Step("validate login")
        public DuSelfcarePage validateLogin() {
            assertEquals(true, this.toolbox.validateLogin());
            return this.toolbox;
        }

        @Step("Search in Du Website:<searchTerm>")
        public void searchInDu(String searchTerm) {
            this.toolbox.searchInDu(searchTerm);
        }

        @Step("Logout")
        public void logout() {
            this.toolbox.logout();
        }
        
        @Step("Change user credentials <newUsername> oldpass <oldpassword> newpass <newPassword> confirmpass: <confirmPassword>")
        public void changeUserPassword(String newUsername, String oldPassword, String newPassword, String confirmPassword){
            this.toolbox.getProfilePreferences().getCredentialsSettingsPage()
                .changeCredentials(newUsername, oldPassword, newPassword, confirmPassword);
        }

        @Step("Export file usage history")
        public void exportUsageHistory() {
            this.toolbox.manageUsageHistory().exportFile();
        }

        @Step("Filter usage history by Date")
        public void getUsageHistoryByIntervalDate() {
            this.toolbox.manageUsageHistory().filterByDate();
        }

        @Step("Go to shop")
        public void goToShopTab(){
            this.toolbox.shopTab();
        }
        
        @Step("Show Usage History by option <value>")
        public void showUsageHistorybyDate(String value){
            this.toolbox.manageUsageHistory().filterUsageHistoryBy(value);
        }

        @Step("Show balance")
        public void getBalance(){
            this.toolbox.showBalance();
        }

        @Step("Load selfcare homepage")
        public DuSelfcarePage homepage(){
            this.toolbox = new DuSelfcarePage(Driver.getDriver(), System.getenv("SELF_CARE_URL"));
            return this.toolbox;
        }

        @Step("Register a new account with number <accountNumber>")
        public void registerNewAccount(String accountNumber){
            this.toolbox.registerAccount().createAccount(accountNumber);
        }
}
