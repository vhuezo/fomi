package com.htc.ngt.lambda.qa.common.testdata;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

public class DataUtil {

	private static final Map<String, String[]> cache = new HashMap<>();

	protected synchronized static String[] lazyread(String key) {
		String[] list = cache.get(key);
		if (null == list) {
			list = DataUtil.read(key);
			cache.put(key, list);
		}
		return list;
	}

	protected static String[] read(String string) {
		return read(string, true);
	}

	protected static String[] read(String name, boolean shuffle) {
		Class<TestData> clazz = TestData.class;
		String path = StringUtils.substringBeforeLast(clazz.getName(), ".");
		path = StringUtils.replace(path, ".", "/");

		try {
			List<String> list = IOUtils.readLines(clazz.getClassLoader().getResourceAsStream(path + "/files/" + name),
					Charset.defaultCharset());

			if (shuffle)
				Collections.shuffle(list);
			return list.toArray(new String[0]);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
