package com.htc.ngt.lambda.qa.common.testdata;

import org.fluttercode.datafactory.NameDataValues;

public class QaNameDataValues implements NameDataValues {
	@Override
	public String[] getSuffixes() {
		return new String[0];
	}

	@Override
	public String[] getPrefixes() {
		return new String[] { "Sr", "Sra." };
	}

	@Override
	public String[] getLastNames() {
		return DataUtil.lazyread("lastnames.txt");
	}

	@Override
	public String[] getFirstNames() {
		return DataUtil.lazyread("males.txt");
	}

}
