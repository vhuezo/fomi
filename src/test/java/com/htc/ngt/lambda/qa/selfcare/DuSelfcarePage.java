package com.htc.ngt.lambda.qa.selfcare;

import com.htc.ngt.lambda.qa.page.AngularPage;
import com.htc.ngt.lambda.qa.selfcare.tabs.account.DuSelfcareNewAccount;
import com.htc.ngt.lambda.qa.selfcare.tabs.manage.DuSelfcareUsageHistory;
import com.htc.ngt.lambda.qa.selfcare.tabs.profile.DuSelfcareProfilePreferences;
import com.htc.ngt.lambda.qa.selfcare.tabs.shop.DuSelfcareShopTab;
import com.htc.ngt.lambda.qa.selfcare.tabs.support.DuSelfcareSupportTab;
import com.htc.ngt.lambda.qa.selfcare.tabs.whyDu.DuSelfcareWhyDuTab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.EnumMap;
import java.util.Map;

/**
 * CSRToolBox
 */
public class DuSelfcarePage extends AngularPage {
    // log

    private final Logger logger = LoggerFactory.getLogger(DuSelfcarePage.class);

    private static By usernameBy = new By.ByCssSelector(".js-txt-username");
    private static By passwordBy = new By.ByCssSelector(".js-txt-password-");
    private static By buttonLoginBy = new By.ByCssSelector("input.btn-primary:nth-child(1)");
    private static By logoduBy = new By.ByCssSelector(".logo > a:nth-child(1) > img:nth-child(1)");
    private static By errorLoginBy = new By.ByCssSelector(".icon-cancel");

    // user menu
    private static By userProfileButton = new By.ByCssSelector(".login > div:nth-child(1)");
    private static By myProfileButton = new By.ByCssSelector(".mya-service-list > li:nth-child(1) > a:nth-child(1)");
    private static By idRegistrationButton = new By.ByCssSelector(
            ".mya-service-list > li:nth-child(2) > a:nth-child(1)");
    private static By pendingRequestButton = new By.ByCssSelector(
            ".mya-service-list > li:nth-child(4) > a:nth-child(1)");
    private static By ticketsButton = new By.ByCssSelector("#formListTicket > li:nth-child(6) > a:nth-child(1)");
    private static By logoutButton = new By.ByXPath("//a[text()='Logout']");
    private static By shopBtn = new By.ByXPath("//a[text()='Shop']");
    private static By whyDubBtn = new By.ByXPath("//a[text()='Why du']");
    private static By supportBtn = new By.ByXPath("//a[text()='Support']");

    // search
    private static By search = new By.ByCssSelector(".search > div:nth-child(1)");
    private static By searchBox = new By.ById("q");
    private static By buttonSearch = new By.ById("btnSearch");

    // balance check
    private static By buttonLastRecharge = new By.ByCssSelector(".last_recharge05");
    private static By buttonUsageHistory = new By.ByCssSelector(".usage_history05");
    private static By buttonIdRegistration = new By.ByCssSelector("div.oneof5:nth-child(4) > a ");
    private static By buttonSavedCreditCard = new By.ByCssSelector(".saved_credit_card05");
    private static By buttonPayForAFriend = new By.ByCssSelector(".pay_for_a_friend05");

    // navigation bar menu
    private static final Map<Tab, By> tabs = new EnumMap<>(Tab.class);

    // others
    private static By skip_btn = new By.ByPartialLinkText("Skip");
    private static By balance = new By.ById("oldPrepaidInfo0");
    private static By register_btn = new By.ById("mya-home-register-btn");

    public enum Tab {
        SHOP, WHYDU, SUPPORT
    }

    static {
        tabs.put(Tab.SHOP, By.xpath("//a[text()='Shop']"));
        tabs.put(Tab.WHYDU, By.xpath("//a[text()='Why du']"));
        tabs.put(Tab.SUPPORT, By.xpath("//a[text()='Support']"));
    }

    public DuSelfcarePage(WebDriver driver, String url) {
        super(driver);
        driver().navigate().to(url);
    }

    public DuSelfcarePage(WebDriver driver) {
        super(driver);
    }

    public DuSelfcarePage logInteraction(String concept) {
        logger.info("");
        logger.info("Running concept: " + concept);
        return this;
    }

    public DuSelfcarePage login(String user, String password) {
        waitForElement(usernameBy, MAX_TIME_WAIT);
        type(usernameBy, user);
        waitForElement(passwordBy, MAX_TIME_WAIT);
        type(passwordBy, password);
        waitForElement(buttonLoginBy, MAX_TIME_WAIT);
        click(buttonLoginBy);
        waitForElement(logoduBy, MAX_TIME_WAIT);
        if (isElementPresent(skip_btn)) {
            click(skip_btn);
        }
        return this;
    }

    public boolean validateLogin() {
        if (els(errorLoginBy).size() > 0) {
            return !el(errorLoginBy).isDisplayed();
        } else {
            return true;
        }

    }

    public DuSelfcareShopTab shopTab() {
        wait(15);
        clickTab(Tab.SHOP);
        waitForAngular();
        return new DuSelfcareShopTab(driver());
    }

    public DuSelfcareWhyDuTab whyDuTab() {
        waitForAngular();
        clickTab(Tab.WHYDU);
        waitForAngular();
        return new DuSelfcareWhyDuTab(driver());
    }

    public DuSelfcareSupportTab supportTab() {
        waitForAngular();
        clickTab(Tab.SUPPORT);
        waitForAngular();
        return new DuSelfcareSupportTab(driver());
    }

    public DuSelfcareShopTab shopBtHover() {
        wait(15);
        Actions action = new Actions(driver());
        WebElement elem = driver().findElement(shopBtn);
        action.moveToElement(elem);
        action.perform();
        return new DuSelfcareShopTab(driver());
    }

    public DuSelfcareWhyDuTab whyDuBtnHover(){
        waitForAngular();
        clickTab(Tab.WHYDU);
        waitForAngular();
        return new DuSelfcareWhyDuTab(driver());
    }

    public DuSelfcareSupportTab supportBtnHover() {
        waitForAngular();
        clickTab(Tab.SUPPORT);
        waitForAngular();
        return new DuSelfcareSupportTab(driver());
    }

    public void logout() {
        wait(10);
        click(userProfileButton);
        wait(10);
        click(logoutButton);
        if (isElementPresent(skip_btn)) {
            click(skip_btn);
        }
        wait(10);
    }

    public void getUserProfileModal() {
        wait(10);
        click(userProfileButton);
        wait(10);
    }

    public DuSelfcarePage searchInDu(String searchTerm) {
        waitForAngular();
        click(search);
        wait(5);
        click(searchBox);
        wait(5);
        type(searchBox, searchTerm);
        click(buttonSearch);
        wt(2000);

        return this;
    }

    public DuSelfcarePage manageLastRecharge() {
        wait(10);
        click(buttonLastRecharge);
        wait(MIN_TIME_WAIT);

        return this;
    }

    public DuSelfcareUsageHistory manageUsageHistory() {
        wait(20);
        executeScript("window.scrollBy(0,400)");
        waitForElement(buttonUsageHistory, MAX_TIME_WAIT);
        click(buttonUsageHistory);
        executeScript("location.reload()");
        waitForAngular();
        return new DuSelfcareUsageHistory(driver());
    }

    public DuSelfcarePage manageIdRegistration() {
        wait(10);
        click(buttonIdRegistration);
        wait(10);
        return this;
    }

    public DuSelfcarePage manageSavedCredit() {
        wait(10);
        click(buttonSavedCreditCard);
        wait(10);

        return this;
    }

    public DuSelfcarePage managePayForAFriend() {
        wait(10);
        click(buttonPayForAFriend);
        wait(10);

        return this;
    }

    // Selects the option of MyProfile under the Profile Preferences icon
    public DuSelfcareProfilePreferences getProfilePreferences() {
        wait(10);
        waitForElement(userProfileButton, MAX_TIME_WAIT);
        click(userProfileButton);
        wait(10);
        waitForElement(myProfileButton, MAX_TIME_WAIT);
        click(myProfileButton);
        return new DuSelfcareProfilePreferences(driver());
    }

    private static By tabs(Tab tab) {
        return tabs.get(tab);
    }

    private void clickTab(Tab tab) {
        waitForAngular();
        el(tabs(tab)).click();
        wait(20);
    }

    public DuSelfcarePage showBalance() {
        wait(8);
        String balan = driver().findElement(balance).getText();
        String nbalan = balan.replaceAll("\n", ",");
        String[] lines = nbalan.split(",");
        String title, balanc, data;
        for (int i = 1; lines.length - 2 > i; i++) {
            if (lines[i].contains("More")) {
                title = lines[i];
                System.out.println("\n----------------------------");
                data = "Tittle:" + title + ":";
            } else {
                balanc = lines[i];
                data = "Balance:" + balanc + ".";
            }
            System.out.println(data);
        }
        wait(8);
        return this;
    }

    public DuSelfcareNewAccount registerAccount(){
        waitForElement(register_btn, MAX_TIME_WAIT);
        click(register_btn);
        return new DuSelfcareNewAccount(driver());
    }
}
