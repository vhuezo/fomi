package com.htc.ngt.lambda.qa.selfcare.tabs.manage;

import java.util.List;
import com.htc.ngt.lambda.qa.selfcare.tabs.DuSelfcareMainTab;
import com.htc.ngt.lambda.qa.util.DateSystem;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DuSelfcareUsageHistory extends DuSelfcareMainTab {

   // private static By exportXLSButton = new By.ByCssSelector("#content > div > div.table-filters > a");
    private static By exportXLSButton = new By.ByXPath("//a[@href='/servlet/duBackend/ExportData?fileName=usage.xls']");
    private static By startDateTxt = new By.ById("startdate");
    private static By endDateTxt = new By.ById("enddate");
    private static By skip_btn = new By.ByCssSelector(
            "body > div.introjs-tooltipReferenceLayer > div > div.introjs-tooltipbuttons > a.introjs-button.introjs-skipbutton");
    private static By all_opt = new By.ByXPath("//*[@id='content']/div/div[4]/div/a[1]");
    private static By voice_opt = new By.ByXPath("//*[@id='content']/div/div[4]/div/a[2]");
    private static By data_opt = new By.ByXPath("//*[@id='content']/div/div[4]/div/a[3]");
    private static By sms_opt = new By.ByXPath("//*[@id='content']/div/div[4]/div/a[4]");
    private static By others_opt = new By.ByXPath("//*[@id='content']/div/div[4]/div/a[5]");
    private static By keyword_filter = new By.ByCssSelector("#usage_filter > label:nth-child(1) > input:nth-child(1)");
    private String dateUsageOld;
    DateSystem dateSystem = new DateSystem();

    public DuSelfcareUsageHistory(WebDriver driver) {
        super(driver);
    }

    public DuSelfcareUsageHistory exportFile() {
        waitForAngular();
        click(exportXLSButton);
        wait(20);
        return this;
    }

    public DuSelfcareUsageHistory filterByDate() {
        try {
            waitForAngular();
            String endDate = dateSystem.getCurrDateAsString("dd/MM/yyyy");
            String startDate = getTableUsageHistory().dateUsageOld;
            if (isElementPresent(skip_btn)) {
                click(skip_btn);
            }
            type(startDateTxt, startDate);
            wait(3);
            type(endDateTxt, endDate);
            wait(3);
            executeScript("javascript:window.scrollBy(0,400)");
            wait(5);
        } catch (Exception e) {
            System.out.println(e + "Action could not be completed");
        }
        return this;
    }

    public DuSelfcareUsageHistory getTableUsageHistory() {
        waitForAngular();
        WebElement table = driver().findElement(By.id("usage"));
        List<WebElement> allRows = table.findElements(By.tagName("tr"));
        if (allRows.size() > 0) {
            for (WebElement row : allRows) {
                List<WebElement> cells = row.findElements(By.tagName("td"));
                for (WebElement cell : cells) {
                    cell.getText();
                    dateUsageOld = cell.getText().substring(0, 10);
                    break;
                }
            }
        }
        return this;
    }

    public DuSelfcareUsageHistory filterUsageHistoryBy(String option) {
        try {
            System.out.println("paso 3");
            switch (option) {
            case "All":
                click(all_opt);
                break;
            case "Voice":
                click(voice_opt);
                break;
            case "Data":
                click(data_opt);
                break;
            case "SMS":
                click(sms_opt);
                break;
            case "Others":
                click(others_opt);
                break;
            case "Keyword":
                String itemData = getTableUsageHistory().dateUsageOld;
                wait(5);
                type(keyword_filter, itemData);
                wait(5);
                break;
            }
            executeScript("javascript:window.scrollBy(0,200)");
            wait(5);
            executeScript("javascript:window.scrollTo(0,0)");
            wait(5);
        } catch (Exception e) {
            System.out.println(e + "Action could not be completed");
        }
        return this;
    }



}
