package com.htc.ngt.lambda.qa.common.testdata;

import org.fluttercode.datafactory.AddressDataValues;

public class QaAddressDataValues implements AddressDataValues {

	@Override
	public String[] getStreetNames() {
		return DataUtil.lazyread("streets.txt");
	}

	@Override
	public String[] getCities() {
		return DataUtil.lazyread("cities.txt");
	}

	@Override
	public String[] getAddressSuffixes() {
		return new String[0];
	}

}
