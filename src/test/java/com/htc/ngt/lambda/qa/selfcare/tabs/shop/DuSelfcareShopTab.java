package com.htc.ngt.lambda.qa.selfcare.tabs.shop;

import com.htc.ngt.lambda.qa.selfcare.tabs.DuSelfcareMainTab;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class DuSelfcareShopTab extends DuSelfcareMainTab {
    private static By shopTab           = new By.ByXPath("//a[text()='Mobile']");

    public DuSelfcareShopTab(WebDriver driver) {
        super(driver);
    }

    public DuSelfcareShopTab goMobileTab() {
		waitForAngular();
		Actions builder = new Actions(driver());
        WebElement element = driver().findElement(shopTab);
		builder.moveToElement(element).build().perform();
		waitForAngular();
		return this;
	}



}
