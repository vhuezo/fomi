package com.htc.ngt.lambda.qa.app;

import static org.junit.Assert.assertTrue;



import com.htc.ngt.lambda.qa.common.StepImplementation;
import com.htc.ngt.lambda.qa.driver.Driver;
import com.thoughtworks.gauge.Step;

/**
 * CSRToolboxSteps
 */
public class CSRToolboxSteps implements StepImplementation {

	private CSRToolboxPage toolbox;

	@Step("go to <url>")
	public CSRToolboxPage goToUrl(String url) {
		this.toolbox = new CSRToolboxPage(Driver.getDriver(true),url);
		return this.toolbox;
	}

	
	@Step("search <parameter>")
	public CSRToolboxPage search(String parameter) {
		
		return this.toolbox.search(parameter);
	}

	@Step("go to first result")
	public CSRToolboxPage searchAndGoToFirstResult() {
		
		return this.toolbox.firstResult();
	}


	@Step("go to fail step")
	public CSRToolboxPage goToFailStep() {
		assertTrue("This is a error to test grab the screen", false);
		return this.toolbox;
	}

	

	
}

