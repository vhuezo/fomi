package com.htc.ngt.lambda.qa.common.testdata;

import java.util.List;
import org.fluttercode.datafactory.impl.DataFactory;
import com.thoughtworks.gauge.Table;

public class TestData {
	private static TestData instance;
	private DataFactory df;

	public String getCity() {
		return df.getCity();
	}

	public String getZip() {
		return String.valueOf(df.getNumberBetween(10000, 99999));
	}

	public TestData() {
		this.df = new DataFactory();
		df.setNameDataValues(new QaNameDataValues());
		df.setAddressDataValues(new QaAddressDataValues());
	}

	public String getFullName() {
		return df.getName();
	}

	public String getFirstName() {
		return df.getFirstName();
	}

	public String getLastName() {
		return df.getLastName();
	}

	public String getStreetName() {
		return df.getStreetName();
	}

	public static synchronized TestData getInstance() {
		if (null == instance) {
			instance = new TestData();
		}
		return instance;
	}

	public static Table introspect(Table data) {
		return new WrappedTable(data);
	}
}

class WrappedTable extends Table {

	public WrappedTable(Table table) {
		super(table.getColumnNames());
		for (List<String> list : table.getRows()) {
			this.addRow(list);
		}
	}
}
