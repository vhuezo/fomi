package com.htc.ngt.lambda.qa.common;

import com.htc.ngt.lambda.qa.driver.Driver;
import com.thoughtworks.gauge.screenshot.ICustomScreenshotGrabber;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class CustomScreenGrabber implements ICustomScreenshotGrabber {
    




    public byte[] takeScreenshot() {
        // DriverFactory is a custom class to manage WebDriver instances
        // and may vary between projects.
        WebDriver driver = Driver.getDriver();
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
}