/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.htc.ngt.lambda.qa.selfcare.tabs.profile;

import com.htc.ngt.lambda.qa.selfcare.tabs.DuSelfcareMainTab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author edenilson
 */
public class DuSelfCareCredentialsSettings extends DuSelfcareMainTab {

    private static final By USERNAME_INPUT = new By.ById("username");
    private static final By OLDPASSWORD_INPUT = new By.ById("oldPassword");
    private static final By PASSWORD_INPUT = new By.ById("password");
    private static final By CONFIRMPASSWORD_INPUT = new By.ByName("confirmPassword");

    public DuSelfCareCredentialsSettings(WebDriver driver) {
        super(driver);
    }

    public DuSelfCareCredentialsSettings changeCredentials(String newUsername, String oldPassword, String newPassword,
            String confirmPassword) {

        // username textbox input
        waitForElement(USERNAME_INPUT, MAX_TIME_WAIT);
        type(USERNAME_INPUT, newUsername);

        // types the oldpassword in the textbox
        waitForElement(OLDPASSWORD_INPUT, MAX_TIME_WAIT);
        type(OLDPASSWORD_INPUT, oldPassword);

        // types the new password in the textbox
        waitForElement(PASSWORD_INPUT, MAX_TIME_WAIT);
        type(PASSWORD_INPUT, newPassword);

        if (confirmPassword.equals(newPassword)) {
            // confirms the password
            waitForElement(CONFIRMPASSWORD_INPUT, MAX_TIME_WAIT);
            type(CONFIRMPASSWORD_INPUT, confirmPassword);
        }else{
            throw new IllegalArgumentException("the entered password doesn't mach with the newpassword field");
        }
        
        return this;

    }   
}
