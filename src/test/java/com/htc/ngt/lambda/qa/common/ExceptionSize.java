
package com.htc.ngt.lambda.qa.common;

public class ExceptionSize extends Exception {

    private static final long serialVersionUID = 6528618562338470614L;
    private long maxSize;
    
    public ExceptionSize(String message,long maxSize){
        super(message);
        this.maxSize = maxSize;
    }

    public long getMaxSize(){
        return this.maxSize;
    }

    public void setMaxSize(long maxSize){
        this.maxSize = maxSize;
    }

}