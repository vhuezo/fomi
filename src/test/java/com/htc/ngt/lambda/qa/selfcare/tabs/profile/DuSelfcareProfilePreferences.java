
package com.htc.ngt.lambda.qa.selfcare.tabs.profile;

import java.util.EnumMap;
import java.util.Map;

import com.htc.ngt.lambda.qa.selfcare.tabs.DuSelfcareMainTab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DuSelfcareProfilePreferences extends DuSelfcareMainTab{
    //Buttons for settings
    private static final By PERSONAL_INFORMATION_BUTTON = new By.ByCssSelector("#formBox1430734227169 > a");
    private static final By LOGIN_CREDENTIALS_BUTTON = new By.ByCssSelector("#formBox1430734227184 > a");
    private static final By LANGUAGE_SETTINGS_BUTTON = new By.ByCssSelector("#formBox1430734227042 > a");
    private static final By PROMOTIONS_BUTTON = new By.ByCssSelector("#formBox1430734226412 > a");
    
    //Enumareted options of the displayed settings at the main page 
    private enum Options {
        PERSONALINFO, CREDENTIALS, LANGUAGE, PROMOTIONS
    }

    //Enum options are mapped
    private static final Map<Options, By> options = new EnumMap<>(Options.class);

    public DuSelfcareProfilePreferences(WebDriver driver) {
        super(driver);
        options.put(Options.PERSONALINFO, PERSONAL_INFORMATION_BUTTON);
        options.put(Options.CREDENTIALS, LOGIN_CREDENTIALS_BUTTON);
        options.put(Options.LANGUAGE, LANGUAGE_SETTINGS_BUTTON);
        options.put(Options.PROMOTIONS, PROMOTIONS_BUTTON);
    }
    

    //Returns the Credentials preferences page
    public DuSelfCareCredentialsSettings getCredentialsSettingsPage() {
        wait(MIN_TIME_WAIT);
        waitForElement(options.get(Options.CREDENTIALS), MAX_TIME_WAIT);
        click(options.get(Options.CREDENTIALS));
        wait(MIN_TIME_WAIT);
        return new DuSelfCareCredentialsSettings(driver());
    }

    //returns the Language preferences page
    public DuSelfCareLanguageSettings getLanguageSettingsPage() {
        wait(MIN_TIME_WAIT);
        waitForElement(options.get(Options.LANGUAGE), MAX_TIME_WAIT);
        click(options.get(Options.LANGUAGE));
        wait(MIN_TIME_WAIT); //MIN_TIME_WAIT equals to 10 seconds on the enviroment variable
        return new DuSelfCareLanguageSettings(driver());
    }

    //returns the Personal Information preferences page
    public DuSelfCarePersonalInfoSettings getPersonalInfoSettingsPage() {
        wait(MIN_TIME_WAIT);
        click(options.get(Options.PERSONALINFO));
        return new DuSelfCarePersonalInfoSettings(driver());
    }

    //returns the promotional message preferences page
    public DuSelfCarePromotionsSettings getPromotionsSettingsPage() {
        wait(MIN_TIME_WAIT);
        click(options.get(Options.PROMOTIONS));
        return new DuSelfCarePromotionsSettings(driver());
    }
}
