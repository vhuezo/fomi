package com.htc.ngt.lambda.qa.selfcare.tabs.account;

import com.htc.ngt.lambda.qa.selfcare.tabs.DuSelfcareMainTab;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DuSelfcareNewAccount extends DuSelfcareMainTab {
  
    private static final By ACCOUNTNUMBER_INPUT = new By.ByCssSelector("#registerForm > fieldset.group.du-text-field > input");

    public DuSelfcareNewAccount(WebDriver driver) {
        super(driver);
    }

    public DuSelfcareNewAccount createAccount(String accountNumber) {
        waitForElement(ACCOUNTNUMBER_INPUT, MAX_TIME_WAIT);
        type(ACCOUNTNUMBER_INPUT, accountNumber);
        wait(15);
        return this;
    }
}
