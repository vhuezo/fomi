package com.htc.ngt.lambda.qa.common;

import java.util.Collection;
import java.util.function.Consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

public class SearchResult {

	private Collection<Object> result;

	public SearchResult(Collection<Object> result) {
		this.result = result;
	}

	public String asJson() {
		try {
			return new ObjectMapper().writeValueAsString(result);
		} catch (JsonProcessingException e) {
			throw new UnsupportedOperationException("Uh, not possible to convert represent as json " + result, e);
		}
	}

	public String asJsonPath(String jsonPath) {
		return JsonPath.parse(asJson()).read(jsonPath).toString();
	}
	
	public DocumentContext jsonPath() {
		return JsonPath.parse(asJson());
	}
	
	public int size() {
		return result.size();
	}

	
	
	public void forEach(Consumer<? super Object> action) {
		result.forEach(action);
	}

}
