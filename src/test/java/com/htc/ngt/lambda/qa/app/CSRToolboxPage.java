package com.htc.ngt.lambda.qa.app;


import com.htc.ngt.lambda.qa.page.AngularPage;
import com.thoughtworks.gauge.Gauge;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CSRToolBox
 */
public class CSRToolboxPage extends AngularPage {
	// log
	private final Logger logger = LoggerFactory.getLogger(CSRToolboxPage.class);

	private static By searchElement = new By.ByName("q");
	private static By buttonSearch	= new By.ByName("btnK");
	private static By linkElement 	= new By.ByCssSelector("#rso > div > div > div:nth-child(1) > div > div > div.r > a:nth-child(1)");
	private static By waitElement 	= new By.ByCssSelector("#preloader"); 
	
	
	

	public CSRToolboxPage(WebDriver driver, String url) {
		super(driver);
		driver().navigate().to(url);
	}

	public CSRToolboxPage(WebDriver driver) {
		super(driver);		
	}



	public CSRToolboxPage logInteraction(String concept) {
		logger.info("");
		logger.info("Running concept: " + concept);
		return this;
	}


	public CSRToolboxPage search(String parameter) {
		waitForElement(searchElement, MAX_TIME_WAIT);
		type(searchElement,parameter);
		waitForElement(buttonSearch, MAX_TIME_WAIT);
		wait(2);
		click(buttonSearch);
		return this;
	}

	public CSRToolboxPage firstResult() {
		waitForElement(linkElement, MAX_TIME_WAIT);
		Gauge.writeMessage(el(linkElement).getAttribute("href").toString());
		driver().navigate().to(el(linkElement).getAttribute("href").toString());
		
		
		while( el(waitElement).getAttribute("style") == null || el(waitElement).getAttribute("style").equals("")) {}
		wait(2);
		
		return this;
	}

	
}
