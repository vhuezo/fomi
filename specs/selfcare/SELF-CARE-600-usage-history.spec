# Self-care-600 Usage history
* Login to selfcare "ecortez@hightech-corp.com" "htc2019du"

## Self care 601 usage history export file  
*  Export file usage history

## Self care 602 usage history by filter date
*  Filter usage history by Date in Selfcare

## Self care 603 usage history by filter By All 
*  Show Usage History by option "All"

## Self care 604 usage history by filter By Voice
*  Show Usage History by option "Voice"

## Self care 605 usage history by filter By Data
*  Show Usage History by option "Data"

## Self care 606 usage history by filter By SMS
*  Show Usage History by option "SMS"

## Self care 607 usage history by filter By Others
* Show Usage History by option "Others"

## Self care 608 usage history by Keyword
* Show Usage History by option "Keyword"


____________________
Teardown steps

* Logout



