#!/bin/bash
REPORT=/ngt-codex-qa/ngt-codex-qa/reports/json-report
FILE=$REPORT/result.json     

# Validate & push the metrics
if [ -f $FILE ]; then
   echo "File $FILE exists."
   cat $REPORT/result.json | \
    # Extract a nice json from last report
    #jq -r '.projectName as $project |.environment as $env | .specResults[] |"ngt_codex_qa {env=\"\($env)\", spec=\"\(.specHeading|gsub("\"";"\\\""))\", result=\"\(.executionStatus)\"} \(.executionTime)"' | \
    jq -r '.projectName as $project |.environment as $env | .specResults[] |.specHeading as $spec| .scenarios[] |.scenarioHeading as $scen|.items[]|.stepText as $step|"ngt_codex_qa{env=\"\($env)\", project=\"\($project|gsub("\"";"\\\""))\", operation=\"du\", spec=\"\($spec|gsub("\"";"\\\""))\", scenario=\"\($scen|gsub("\"";"\\\""))\", step=\"\($step|gsub("\"";"\\\""))\", result=\"\(.result.status)\"}  \(.result.executionTime)"' | \
    # Send to the metric server
    curl -v --data-binary @- $PUSH_GATE/metrics/job/ngt
    name=`uname -n`
    timestamp=`date +%s`
    result=`jq -r '.executionStatus' $REPORT/result.json`
    echo "prometrics_exec{name=\"$name\", result=\"$result\"} $timestamp" | curl --data-binary @- $PUSH_GATE/metrics/job/ngt_prometrics
else
   echo "File $FILE does not exist."
fi